//
//  customeViewCell.swift
//  apiTest1
//
//  Created by Hamza on 1/21/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class customeViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    
}
